### No 1 Membuat Database
create database myshop;

### No 2 Membuat Table di dalam database

## Table Users
create table users (id int(11) auto_increment,name varchar(255),email varchar(255),password varchar(255),primary key(id));

## Table Items
create table items(id int(11) auto_increment,name varchar(255), description varchar(255), price int(11), stock int(11), category_id int(11),primary key(id), foreign key(category_id) references categories(id));

## Table Categories
create table categories(id int(11) auto_increment,name varchar(255),primary key(id));

### No 3 Memasukkan Data pada Table

## Table users
insert into users(name,email,password) values ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

## Table categories
insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

## Table items
insert into items(name,description,price,stock,category_id) values ("Sumsang b50","Hape keren dari merek sumsang","4000000","100","1"),("Uniklooh","Baju keren dari brand ternama","500000","50","2"),("IMHO Watch","Jam tangan anak ynag jujur banget","2000000","10","1");

### No 4 Mengambil Data dari Database

## Mengambil Data Users semua field kecuali password
select id, name, email from users;

## Mengambil data Items

# Items yang memiliki harga di atas 1000000
select * from items where price >1000000;

# Items yang memiliki name serupa atau mirip 'uniklo'
select * from items where name like '%uniklo%';

## Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items join categories on items.category_id = categories.id;

### No 5 Mengubah Data dari Database

update  items set price = 2500000 where id = 1;



















