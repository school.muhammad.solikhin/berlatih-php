<?php
require_once "animal.php";

    class Frog extends Animal
    {
        public $nama;
        public $jumlah_kaki = 4;
        public $berdarah_dingin = false;

        public function __construct($name)
        {
            $this->nama = $name;
        }

        public function jump()
        {
            echo "Hop Hop";
        }
    }    

?>