<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $domba = new Animal('Domba');
    echo "<h3> Release 0 </h3>";
    echo "Nama Hewan : ".$domba->nama."<br>";
    echo "Jumlah Kaki : ".$domba->jumlah_kaki."<br>";
    if ($domba->berdarah_dingin) {
        echo "Berdarah Dingin : Ya <br>";
    }
    else {
        echo "Berdarah Dingin : Tidak <br>";
    }

    echo "<h3> Release 1 </h3>";
    $kodok = new Frog("Buduk");
    echo "Nama Hewan : ".$kodok->nama."<br>";
    echo "Jumlah Kaki : ".$kodok->jumlah_kaki."<br>";
    if ($kodok->berdarah_dingin) {
        echo "Berdarah Dingin : Ya <br>";
    }
    else {
        echo "Berdarah Dingin : Tidak <br>";
    }
    echo " => Jump".$kodok->jump();
    echo "<br><br>";
    $kera = new Ape("Kera Sakti");
    echo "Nama Hewan : ".$kera->nama."<br>";
    echo "Jumlah Kaki : ".$kera->jumlah_kaki."<br>";
    if ($kera->berdarah_dingin) {
        echo "Berdarah Dingin : Ya <br>";
    }
    else {
        echo "Berdarah Dingin : Tidak <br>";
    }
    echo " => Yell".$kera->yell();
?>