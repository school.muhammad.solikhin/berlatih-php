<?php
require_once "animal.php";

    class Ape extends Animal
    {
        public $nama;
        public $jumlah_kaki = 2;
        public $berdarah_dingin = false;

        public function __construct($name)
        {
            $this->nama = $name;
        }

        public function yell()
        {
            echo "Auooo";
        }
    }    

?>